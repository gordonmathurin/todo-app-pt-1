import React, { Component } from "react";
import todosList from "./todos.json";
import { v4 as uuidv4 } from 'uuid';



class App extends Component {
  state = {
    todos: todosList,

  };

  handleAddTodo = (event) => {
    let newTodos = {
      "userId": 1,
      "id": uuidv4(),
      "title": event.target.value,
      "completed": false
    }
    if (event.key === "Enter") {
      console.log(newTodos)
      this.setState({ todos: [...this.state.todos, newTodos] })
      event.target.value = ''
    }
  }

  handleMarkTodo = (id) => {
    let newTodos = this.state.todos.map((todo) => {
      if (todo.id === id) {
        return {
          ...todo,
          completed: !todo.completed
        }
      }
      return todo;
    })
    this.setState((state) => {
      return {
        ...state,
        todos: newTodos
      }

    })
  }

  handleDelete = (event, id) => {
    const newTodoList = this.state.todos.filter((todo) => todo.id !== id)
    this.setState({ todos: newTodoList })
  }

  handleDeleteAll = (event) => {
    const newTodoList = this.state.todos.filter((todo) => todo.completed === false)
    this.setState({ todos: newTodoList })
  }

  render() {
    return (
      <section className="todoapp">
        <header className="header">
          <h1>todos</h1>
          <input className="new-todo" placeholder="Enter Next Item" onKeyPress={this.handleAddTodo} value={this.state.input} autofocus />
        </header>
        <TodoList todos={this.state.todos}
          handleMarkTodo={this.handleMarkTodo}
          handleDelete={this.handleDelete}
          handleDeleteAll={this.handleDeleteAll} />

        <footer className="footer">
          <span className="todo-count">
            <strong>0</strong> item(s) left
          </span>
          <button className="clear-completed" onClick={this.handleDeleteAll}>Clear completed</button>
        </footer>
      </section>
    );
  }
}

class TodoItem extends Component {

  render() {
    return (
      <li className={this.props.completed ? "completed" : ""}>
        <div className="view">
          <input className="toggle" type="checkbox"
            checked={this.props.completed}
            onChange={this.props.handleMarkTodo} />
          <label>{this.props.title}</label>
          <button className="destroy"
            onClick={this.props.handleDelete} />
        </div>
      </li>
    );
  }
}

class TodoList extends Component {
  render() {
    return (
      <section className="main">
        <ul className="todo-list">
          {this.props.todos.map((todo) => (
            <TodoItem
              key={todo.id}
              title={todo.title}
              completed={todo.completed}
              handleMarkTodo={event => this.props.handleMarkTodo(todo.id)}
              handleDelete={event => this.props.handleDelete(event, todo.id)}
            />
          ))}
        </ul>
      </section>
    );
  }
}



export default App;

//worked with Greg, Erica, and Morgan